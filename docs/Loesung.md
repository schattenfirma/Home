**Welche Hinweise (entschlüsselt) wurden hinterlassen?**

       Lösung:  MEETING
                25042028


Die RFID und den QR-Code Scannen (SKKZOTM und 74957971) und in den [Decoder](https://replit.com/@YamsBond/Decoder) in die Variablen "verschluesselt" jeweils SKKZOTM beim worddecoder, 74957971 bei numberdecoder eintragen.

Dann auf "Run" klicken.

Anmerkung: Replit wird voraussichtlich auf eine [Jupyter-Instanz](https://jupyter.hacken.online) umgezogen, da dann kein Account nötig ist.

Daraus leitet sich ein Meeting am 25.04.2028 ab. 

**Zu welcher Applikation führten die ersten Hinweise?**

        Lösung: Kalender

Im Kalender ist ein Meeting am 24.04.2028 eingetragen. Im Meetingtext findet sich dann die [Website](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/)

**Wie heißt die Firma, für die die Person arbeitet?**
 
        Lösung: Schattenfirma 

Steht direkt auf der Startseite

**Welches Soziale Netzwerk nutzt die Person?**

        Lösung: Mastodon

im [replit unter Vorbereitung](https://replit.com/@YamsBond/Decoder?v=1#Vorbereitung.md) gibt es den Hinweis den HTML-Code zu durchleuchten. 
Auf der [Schattenfirma-Website](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/) findet sich ein Kommentar:


> \<!---Testuser: https://mastodon.social/@yams_bond --\\> 

Dieser führt auf Mastodon


**Wie nennt sich die Plattform, auf der die Person ihren Code postet?**

        Lösung: Github

Auf dem [Mastodon-Profil](https://mastodon.social/@yams_bond) ist ein Link zu [Github](https://github.com/YamsBond/SMTSpyPlan/)



**Welche Login-Daten wurden gefunden?**

        Lösung: yams_bond pass
                matahari Caesar1876

Von der vorherigen frage haben wir den Hinweis 

 > \<!---Testuser: https://mastodon.social/@yams_bond --\\>

Ein User ist also yams_bond

Auf [Github](https://github.com/YamsBond/SMTSpyPlan/blob/main/main.py) findet sich im Code folgender Absatz:

        def cmatrix_animation():
                cmatrix = [
                "Wake up, Neo...",
                "The Matrix has you...",
                "Follow the white rabbit.",
                "Knock, knock, Neo.", 
                "Password: pass"
                ]

Ein Password ist somit pass

Auf Mastodon wird ein User matahari genannt mit Hinweisen zu dem Haustier Caesar und dem Geburtsjahr 1876. Mit Enumeration (und dem Jimmy Kimmel Video) kommt man auf das Passwort Caesar1876

**Wie kann man die Dienstleistungen einsehen?**

        Lösung: dienstleistungen.html

Am Ende der URL muss in "dienstleistungen-html" der "-" durch einen "." ausgetauscht werden. So kann die [Dienstleistungen-Seite](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/dienstleistungen.html) eingesehen werden.
Da die URL die Adresse zu einer Datei ist, und eine Datei mit name.dateiendung geschrieben wird, ist die Seite mit einem "-" nicht mehr findbar.

**Wie heißt die Person?**

        Lösung: Elsbeth Schragmueller
Auf der [Secretpage2](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/secretpage2.html) (login mit matahari) ist ein Bild zu finden. 
Dieses muss heruntergeladen werden und auf [exif.tools](https://exif.tools/) hochgeladen werden. Unter "Artist" wird dann der Name von Elisabeth Schragmueller angezeigt.

Der Hinweis auf das Tool findet sich auf dem Mastodon-Profil. 

**Wo findet man die Dienstleistungen der Person (Stadt)?**

        Lösung: Jena
Auf der [Dienstleistungen-Seite](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/dienstleistungen.html) findet sich ein Binärcode. Wenn man "Binär zu Text" googled, sollte sich ein [Converter](https://www.rapidtables.com/convert/number/binary-to-ascii.html) finden. 
Nach der Umrechnung kommen dann Koordinaten heraus: 
50.928172 N 11.587936 E

diese in Maps eingegeben zeigen Jena an. 

**Wie erhielt die Person Zugang?**

        Lösung: I came with a bug on a backdoor

Auf [Secretpage1](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/secretpage1.html) (login mit yamsbond)  erhält man den Hinweis "Die Wahrheit liegt immer in den Keksen". Dazu muss man die ganze Seite Markieren zB mit Strg+A. 
Die Anleitung zum Cookies auslesen in Edge findet sich im [Github](https://github.com/YamsBond/SMTSpyPlan/blob/main/README.md)

Der Cookie "How I came in: I came with a bug on a backdoor" wird beim erstmaligen Login gesetzt. 


**Secretflag**

Beim Ausprobieren kann [Secretpage3](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/secretpage3.html) gefunden werden. 

