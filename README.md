 CTF - Schattenfirma

Dieses Repository enthält alle benötigten Quellen und die Dokumentation für ein Capture the Flag (CTF). 

## Level
- Anfänger:innen (z. B. Jugendliche) die mit dem Umgang mit PCs und Web-Browsern vertraut sind .
- Einsteiger in die Themen OSINT und Web-Analyse.

## Ziel
- Die Aufgabe des CTF ist es einen Wirtschaftsspion zu identifizieren.
- Lernziele sind OSINT, Web-Analyse und die Einführung in CTFs und IT-Security.

## Inhalt

### Website

Unter [public/](public/) befindet sich eine Website, die per [GitLab CI][ci] (mit der [.gitlab-ci.yml](.gitlab-ci.yml)) 
erstellt und von Gitlab gehostet wird. Die Website befindet sich unter 
[home-schattenfirma-<nr>.gitlab.io](https://home-schattenfirma-037ab2c09006c43fc3c489b644ef0cf4c0d1d4c4826c.gitlab.io/). 

### Dokumentation

Unter [docs](docs) befindet sich die "Dokumentation" des Projekts in Form eines Walktroughs als Bild.

## Links
- Selbst verwaltet: 
  - [gitlab.com/schattenfirma/Home](gitlab.com/schattenfirma/Home)
  - [mastodon.social/@yams_bond](mastodon.social/@yams_bond)
  - [https://replit.com/@YamsBond/Decoder](https://replit.com/@YamsBond/Decoder)
- Externe Websites:
  - [exif.tools](exif.tools)
  - [openstreetmap.org](openstreetmap.org)
  - [convertbinary.com](de.convertbinary.com/binaercode-in-text-umwandeln)
  - [microsoft.com/<...>/cookies](learn.microsoft.com/de-de/microsoft-edge/devtools-guide-chromium/storage/cookies)

  ## Todo
- Code für das Replit in Gitlab integrieren
- Code für das Replit automatisch ausliefern

##Preptodos:
1. get Laptops + wifi
1. Check repl for hints
2. prep rfid + qr code 
3. make Calendarentry

## story

1. get hint SKKZOTM and RFID to repl.it
2. get hint 74957971 in QR code
3. find link to schattenfirma webite in calendar
4. find clue for username and mastodon profile in comments
6. find repo on mastodon profile
7. find leaked password for website in repo
8. login to website and downlod image
9. read EXIF to find Spyname
10. get location from dienstleistungen




 
